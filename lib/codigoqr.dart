
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class Codigoqr extends StatefulWidget {
  @override
  _CodigoqrState createState() => _CodigoqrState();
}

class _CodigoqrState extends State<Codigoqr> {
  String barcode = '';
  Uint8List bytes = Uint8List(200);


  @override
  initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text("Cafeteria UMSA"),),
          backgroundColor: Color(0xFF388E3C),
         
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("BIENVENIDO", style: TextStyle(fontSize: 45,fontWeight: FontWeight.bold),),
              Text("Escanea nuestro codigo y tendras", style: TextStyle(fontSize: 20,)),
              Text("acseso a nuestro Menu", style: TextStyle(fontSize: 20,)),
              SizedBox(
                width: 200,
                height: 200,
                child: Image.memory(bytes),
              ),
              Text('RESULT  $barcode', style: TextStyle(fontSize: 23),),
              RaisedButton(
                color: Colors.lightGreen,
                onPressed: _scan, child: Text("Scanear",style: TextStyle(color: Colors.white, fontSize: 20,fontWeight: FontWeight.normal),)),
              RaisedButton(
                color: Colors.lightGreen,
                onPressed: _generateBarCode, child: Text("Generar Codigo",style: TextStyle(color: Colors.white, fontSize: 20,fontWeight: FontWeight.normal))),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.lightGreen,
          onPressed: ()=> Navigator.of(context).pushNamed("menu"), label: Text("MENU"), 
          icon: Icon(Icons.restaurant_menu),),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      );
  }
   Future _scan() async {
    String barcode = await scanner.scan();
    setState(() => this.barcode = barcode);
  }

  Future _generateBarCode() async {
    Uint8List result = await scanner.generateBarCode('https://github.com/leyan95/qrcode_scanner');
    this.setState(() => this.bytes = result);
  }
}
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:qrdocu/codigoqr.dart';
import 'package:qrdocu/menu.dart';
import 'package:qrscan/qrscan.dart' as scanner;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
       // primarySwatch: Colors.orange,
        //primaryColorLight: Colors.red
       // primaryColorDark: Colors.black
      ),
      initialRoute: "codigoqr",
      //home: Menu(),
      routes: {
        "codigoqr": (BuildContext context)=> Codigoqr(),
        "menu": (BuildContext context)=> Menu(),
      },
    );
  }
}


  

import 'package:flutter/material.dart';
class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    //vitas de las pnatllas
    final _tablas=<Widget>[
      desayunos(),
      almuerzos(),
      //Icon(Icons.star),
     snack(),
    ];
    // las tablas
    final _iconosdepa =<Tab>[
      Tab(icon: Icon(Icons.local_cafe,),text: "Desayuno",),
      Tab(icon: Icon(Icons.restaurant),text: "Almuerzo",),
      Tab(icon: Icon(Icons.fastfood),text: "Snack",),
    

    ];

    return DefaultTabController(
      length: _iconosdepa.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Menu"),
          backgroundColor: Color(0xFF388E3C),
          bottom: TabBar(tabs: _iconosdepa),
        ),
        body: TabBarView(
          children: _tablas,

        ),
      
        ),


      
    );
  }
  Widget desayunos(){
    return SingleChildScrollView(
      //width: 200,
      //color: Colors.blue,
      child: Container(
        //color: Colors.blue,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Bebidas", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Cafe", "imagenes/cafe.jpeg"),
           _item("Te", "imagenes/te.jpeg"),
           _item("Platano con Leche", "imagenes/batidopla.jpeg"),
           _item("Frutilla con Leche", "imagenes/batidofru.jpeg"),
           Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Desayunos Completos", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Ensalada de Frutas", "imagenes/frutas.jpeg"),
           _item("Hot Cakes", "imagenes/hotkay.jpeg"),
           _item("Desayuno Americano", "imagenes/desayunoa.jpg"),
           _item("Desayuno Yungueño", "imagenes/des2.jpeg"),
           



           

          ],
        ),
      ),
      


    );
  }
  Widget almuerzos(){
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
             Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Aperitivo", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Entrada", "imagenes/entrada.jpeg"),
           Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Sopa", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Sopa de Mani", "imagenes/sopa.jpeg"),
           Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Segundos", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Sajta de Pollo", "imagenes/segundo2.jpeg"),
           _item("Piquemacho", "imagenes/segundo1.jpeg"),
           Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Postre", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Postre", "imagenes/postres.jpeg"),

          ],
        ),
      ),
      


    );
  }
  Widget snack(){
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
             padding: EdgeInsets.only(top: 12,left: 23),
             child: Text("Platos Extras", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Salchipapa", "imagenes/salchipapa.jpeg"),
           _item("Alitas de Pollo", "imagenes/alipollo.jpeg"),
           _item("Hamburguesa", "imagenes/hamburguesa.jpeg"),
           _item("Hot Dog", "imagenes/hotdog.jpeg"),
           _item("Pizza Carnivora", "imagenes/pizza.jpeg"),
           
           Container(
             padding: EdgeInsets.only(top: 12, left: 23),
             child: Text("Bebidas", style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),),
           ),
           _item("Coca Cola", "imagenes/coca.jpeg"),
           _item("Pepsi", "imagenes/pepsi.jpeg"),
           _item("Jarra de Jugo", "imagenes/jarra.jpeg"),
           
          ],
        ),
      ),
      


    );
  }
  Widget _item(String titulo, String url){
    return Card(
      color: Colors.lightGreen,
      elevation: 10,
      margin: EdgeInsets.only( top: 25,right: 20,left: 20, bottom: 0
      ),
      child: Row(
        children: <Widget>[
           Image(
            image: AssetImage(url),
            width: MediaQuery.of(context).size.width/2.3,
            height: MediaQuery.of(context).size.height/4,
            fit: BoxFit.cover,),
            //SizedBox(width: 50,),
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Column( 
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                Center(
                    child:Text(titulo, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 17
                    ,fontWeight: FontWeight.bold),) ,
                  ),
                  SizedBox(height:15 ,),
                  Text("-Precio:     23 Bs", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),),
                  
               // SizedBox(height: 32,),
                Container(
                  padding: EdgeInsets.only(top:63,left: 34,),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.star, color: Colors.orangeAccent,size: 20,),
                      Icon(Icons.star, color: Colors.orangeAccent,size: 20,),
                      Icon(Icons.star, color: Colors.orangeAccent,size: 20,),
                      Icon(Icons.star, color: Colors.orangeAccent,size: 20,),
                      Icon(Icons.star, color: Colors.orangeAccent,size: 20,),
                    //IconButton(color: Colors.yellow,icon: Icon(Icons.star), onPressed:(){})
                    ],
                  ),
                )
              ],),
            ),
        ],


      )
    );

  }
 
}